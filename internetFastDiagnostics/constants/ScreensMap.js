import React from 'react';
import {CrashStep} from "../components/stepScreens/CrashStep";
import {CableDiagStep} from "../components/stepScreens/CableDiagStep";

const ScreensMap = new Map([
    [0, {
        title: 'Аварии',
        component: (props) => <CrashStep {...props}/>
    }],
    [1, {
        title: 'Диагностика',
        component: (props) => <CableDiagStep {...props}/>
    }]
]);

export function getScreenById (id) {
    return ScreensMap.has(id) ? ScreensMap.get(id) : <ErrorScreen />
}
