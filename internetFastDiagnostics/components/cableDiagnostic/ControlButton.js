import React from 'react';
import Spinner from "../../../../Spinner";

/**
 * Общий компонент "Большая кнопка"
 * @param handleClick
 * @param buttonText
 * @param isLoading
 * @returns {*}
 * @constructor
 */
export const ControlButton = ({handleClick, buttonText, isLoading}) =>
    <button className={'btn btn-primary mgbottom10'}  style={{width: '100%'}} onClick={() => handleClick()} disabled={isLoading}>
        {isLoading ? <Spinner radius={10} style={{marginTop: '4px'}}/> : buttonText}
    </button>;