import React, {useState} from 'react';
import {ControlButton} from "./ControlButton";
import {routerControlApi} from "../../../request/RouterControlApi";
import {TECHSUP_URL} from "../../../../helpers/JiraLink";
import {DEFAULT_TECHSUP_DESCRIPTION} from "../../constants";

/**
 * компонент взаимодействия с клиентским оборудованием
 * кнопка перезагрузки роутера + ссылка на создание тикета Jira
 * @param props
 * @returns {*}
 * @constructor
 */

export default function RouterControl(props){

    const {isLoading, fastDiag, contractNumber, subscriptionId} = props;

    const {router: {data}, withCache} = fastDiag;

    const [showButton, setButton] = useState(true);

    const rebootParameter = (device) => {
        return {
            action: 'reboot',
            id: device.id
        }
    };

    function reboot() {
        if (confirm('Вы уверены, что хотите перезагрузить роутер?')){
            data.forEach(device => {
                routerControlApi.postChangeParameter(rebootParameter(device))
                    .then(
                        (resp) => {
                            alert('Задача отправлена на роутер. Перезагрузка может занять некторое время.');
                            setButton(false);
                        },
                        (err) => {
                            console.warn('Ошибка RouterControl: ', err);
                            alert('Ошибка, роутер не был перезагружен');
                        })
            })
        }
    }

    const ticketLink = <p className={'muted'}>Проверьте связь после перезагрузки.<br /> Проверьте подключение на устройствах абонента.<br /> Если интернет отсутствует, сформируйте заявку <a href={`${TECHSUP_URL(contractNumber)}/39/${subscriptionId}?autoTherapy=1&description=${DEFAULT_TECHSUP_DESCRIPTION}${withCache ? '&withCache=1' : ''}` } target='_blank'>Отсутствие связи,  проблемы  со связью</a></p>

    return showButton
        ? <ControlButton isLoading={isLoading || fastDiag.isLoading} handleClick={() => reboot()} buttonText={'Перезагрузить роутер'}/>
        : ticketLink
}
