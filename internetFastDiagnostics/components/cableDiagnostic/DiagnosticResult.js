import React from 'react';
import PropTypes from 'prop-types';

export const DiagnosticResult = (props) => {

    const {testResult} = props;

    return testResult.map(block =>
        <div className="row-fluid" key={block.title}>
            <div className="span">
                <ul className="inline">
                    <li>{block.title}</li>
                    {block.tests.map((test, key) => <li key={`${block.title}-${key}`}>{test}</li>)}
                </ul>
            </div>
        </div>
    )

};

DiagnosticResult.propTypes = {
    testResult: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        tests: PropTypes.array
    })).isRequired
};
