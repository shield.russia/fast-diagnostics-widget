import React, {useEffect} from 'react';
import {autoTherapyApiV3} from '../../../request/AutoTherapyApiV3';
import {SWITCH_CRASH_TYPE, APPS, SUCCESS_STATE} from "../../constants";
import {TECHSUP_URL} from "../../../../helpers/JiraLink";
import moment from "moment";
import {ControlButton} from "./ControlButton";

/**
 * Компонент автолечения
 * Показывает последний результат автолечения из других приложений
 * взаимодействует с api автолечения
 *
 * @param props
 * @returns {*}
 * @constructor
 */
export default function AutoResurrection(props) {

    const {
        resurrection: {
            isLoading,
            lastResult,
            error,
            setLoading,
            setData,
            data,
            setError,
            message,
            setMessage,
            checkRouter
        },
        fastDiag,
        subscriptionId,
        contractNumber,
        crashesProps: {crashes}
    } = props;

    useEffect(() => {
        if (lastResult && lastResult.length > 0) {
            const ivrLaunches = lastResult.filter(session => APPS.includes(session.app));
            if (ivrLaunches.length > 0) {
                const lastLaunch = ivrLaunches.find(session => {
                    const {date} = session;
                    return moment().diff(moment(date), 'minutes') < 10
                });
                if (lastLaunch) {
                    const lastIndex = lastLaunch.logs.length - 1;
                    setData(lastLaunch.logs[lastIndex].description);
                    setMessage(lastLaunch.result_state);
                }
            }
        }
    }, [lastResult]);

    const description = data && data.replace(/\r?\n/g, "br");

    const switchErrorTicket = crashes.length
        ? crashes.find(crash => crash.issueTypeId === SWITCH_CRASH_TYPE)
        : null;

    const getCrashTicketLink = switchErrorTicket
        ?
        <a href={`${TECHSUP_URL(contractNumber)}/269/${subscriptionId}?preset=no_ping_crash_found&pkey=${switchErrorTicket}&autoTherapy=1&description=${description}`}>создать
            подзадачу</a>
        :
        <a href={`${TECHSUP_URL(contractNumber)}/39/${subscriptionId}?preset=no_ping&autoTherapy=1&description=${description}`}>создать
            тикет</a>;

    const messages = {
        2: <p className={'muted'}>Сформируйте заявку <a href={`${TECHSUP_URL(contractNumber)}/39/${subscriptionId}?autoTherapy=1&description=${description}`}>Отсутствие связи,  проблемы  со связью</a></p>,
        3: <p className={'muted'}>Сформируйте заявку  <a href={`${TECHSUP_URL(contractNumber)}/39/${subscriptionId}?autoTherapy=1&description=${description}&repair=1`}>на ремонт</a></p>,
        4: <p className={'muted'}>Недоступен коммутатор, требуется {getCrashTicketLink}</p>,
        5: <p className={'muted'}>Неисправен порт на коммутаторе. Сформируйте заявку <a href={`${TECHSUP_URL(contractNumber)}/39/${subscriptionId}?autoTherapy=1&description=${description}&repair=1`}>на замену порта</a></p>
    };

    function onResurrectionResponse(response) {
        if (response.state === SUCCESS_STATE){
            setTimeout(() => {
                checkRouter();
                setResurrectionData(response);
                }, 5000)
        } else {
            setResurrectionData(response);
        }
    }

    function setResurrectionData(resp){
        setLoading(false);
        setError(null);
        setData(resp.description);
        setMessage(resp.state)
    }

    function onResurrectionReject(error) {
        console.warn('Auto Therapy error: ', error);
        setLoading(false);
        setError(error);
    }

    function startResurrection() {
        setLoading(true);
        autoTherapyApiV3
            .resurrection(subscriptionId)
            .then(onResurrectionResponse, onResurrectionReject);
    }

    const renderData = data ? data : null;

    const renderError = error ? <p className={'text-warning mgtop10'}>{error}</p> : '';

    const renderMessage = getMessage(message) ? getMessage(message) : '';

    function getMessage(state) {
        if (state){
            return messages[Number(state)] || false
        }
        else {
            return false
        }
    }

    return <div className={'panel'}>
        <ControlButton isLoading={isLoading || fastDiag.isLoading} handleClick={() => startResurrection()} buttonText={'Запустить автолечение'}/>
        <div className={'mgtop10 text-center'}>
            {renderData}
            {renderMessage}
            {renderError}
        </div>
    </div>

}