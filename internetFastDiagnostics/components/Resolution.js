import React from 'react';

/**
 * Компонент содержит результат работы быстрой диагностики
 * @param type
 * @param children
 * @param props
 * @returns {*}
 * @constructor
 */
export const Resolution = ({type, children, ...props}) => <span className={`label label-${type}`} {...props}>{children}</span>;