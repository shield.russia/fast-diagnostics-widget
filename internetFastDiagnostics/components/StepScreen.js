import React, {useEffect, useState} from 'react';
import moment from "moment";
import {faultAPI} from "../../request/FaultAPI";
import {getScreenById} from "../constants/ScreensMap";
import {autoTherapyApiV3} from "../../request/AutoTherapyApiV3";
import {routerControlApi} from "../../request/RouterControlApi";

/**
 * Компонент содержит в себе экраны-"шаги" быстрой диагностики
 * является основным компонентом. хранящим состояния для экранов
 * @param props
 * @returns {*|never}
 * @constructor
 */
export default function StepScreen(props) {

    const {subscriptionId, ipAddress} = props;

    const [diagResult, setDiagResult] = useState(null);
    const [isDiagLoading, setDiagLoading] = useState(true);
    const [diagError, setDiagError] = useState(null);
    const [lastResurrection, setLastResurrection] = useState(null);
    const [resurrectionResult, setResurrectionResult] = useState(null);
    const [isResurrectionLoading, setResurretionLoading] = useState(false);
    const [resurrectionError, setResurrectionError] = useState(null);
    const [resurrectionMessage, setResurrectionMessage] = useState(null);
    const [crashes, setCrashes] = useState([]);
    const [loadingCrashes, setLoadingCrashes] = useState(true);
    const [crashesHistory, setCrashesHistory] = useState([]);
    const [loadingHistory, setLoadingHistory] = useState(true);
    const [hasRouter, setRouter] = useState(false);
    const [routerState, setRouterState] = useState(false);
    const [router, addRouter] = useState([]);
    const [withCache, setCache] = useState(false);

    function getRouterState(){
        routerControlApi.checkState(ipAddress)
            .then(
                (resp)=>{
                    const routerExist = resp.hasOwnProperty('is_available');
                    setRouter(routerExist);
                    setRouterState(routerExist ? resp.is_available : false);
                },
                (err)=>{
                    setRouter(false);
                    setRouterState(false);
                });
    }

    useEffect(() => {
        autoTherapyApiV3.getStatistics(subscriptionId)
            .then(setLastResurrection, setResurrectionError);

        faultAPI.getCrashes([subscriptionId])
            .then(resp => {handleResponse(resp, setCrashes, setLoadingCrashes)})
            .catch(err => {console.warn("FAULT ERROR: ", err); handleResponse([], setCrashes, setLoadingCrashes);});

        faultAPI.getHistory([subscriptionId], moment().subtract(5, 'days').format("YYYY-MM-DD HH:00:00"), moment().format("YYYY-MM-DD HH:00:00"))
            .then(resp => {handleResponse(resp, setCrashesHistory, setLoadingHistory)})
            .catch(err => {console.warn("FAULT ERROR: ", err); handleResponse([], setCrashesHistory, setLoadingHistory);});

        getRouterState();

    }, [subscriptionId]);

    useEffect(() => {
        routerControlApi.getRouterByIp(ipAddress)
            .then((resp) => {
                addRouter(resp);
            },(err)=>{
                console.warn('Router API error: ', err);
                addRouter([]);
            })
    }, [hasRouter]);

    function handleResponse(data, set, setLoading) {
        set(data);
        setLoading(false);
    }

    const screen = getScreenById(props.selected);

    const crashesProps = {
        crashesHistory,
        crashes,
        isLoading: loadingCrashes || loadingHistory
    };

    const diagProps = {
        setResult: setDiagResult,
        setLoading: setDiagLoading,
        setError: setDiagError,
        result: diagResult,
        isLoading: isDiagLoading,
        isError: diagError,
        withCache,
        setCache,
        hasRouter,
        routerState,
        router,
        checkRouter: () => getRouterState()
    };

    const resurrectionProps = {
        isLoading: isResurrectionLoading,
        setLoading: setResurretionLoading,
        data: resurrectionResult,
        setData: setResurrectionResult,
        error: resurrectionError,
        setError: setResurrectionError,
        lastResult: lastResurrection,
        message: resurrectionMessage,
        setMessage: setResurrectionMessage,
        checkRouter: () => getRouterState()
    };

    return screen.component({...props, crashesProps:crashesProps, fastDiag: diagProps, resurrection: resurrectionProps});
}