import React from 'react';
import moment from "moment";
import {TECHSUP_URL} from "../../../../helpers/JiraLink";
import {SUBTASK_DESCRIPTION} from "../../helpers";
import {SWITCH_CRASH_TYPE} from "../../constants";
import Spinner from "../../../../Spinner";

/**
 * Шаг аварий
 * Выдается первым, если аварий нет - пропускается
 * Отображает текущие аварии. влияющие на точку доступа с уровнем влияния "Полное отсутствие"
 * Отображает историю аварий, влиявших на точку доступа с уровнем влияния "Полное отсутствие"
 * @param props
 * @returns {null|*}
 * @constructor
 */
export const CrashStep = (props) => {

    const {crashesProps: {crashes: crashesActual, crashesHistory: crashesClosed, isLoading}, setLast, remove, set, subscriptionId} = props;

    setLast(props.selected + 1);

    if (isLoading){
        return <Spinner />
    }

    const crashes = filteredCrashes(crashesActual);
    const crashesHistory = filteredCrashes(crashesClosed);

    function filteredCrashes(crashList) {
        return crashList
            .filter(crash => {
                return crash.subscriptionIds.includes(+subscriptionId)
                    && crash.serviceInfluences.find(influence =>
                        influence.influenceName === "Полное отсутствие"
                        && influence.serviceName === "Интернет"
                    )
            })
    }

    if (!crashes.length && !crashesHistory.length) {
        remove(props.selected);
        set(props.selected + 1);
        return null;
    }

    const crashTable = (crashList, isActual) => crashList.map(crash => <TableRow key={crash.externalId} crash={crash}
                                                                                 isActual={isActual} {...props}/>)

    const isActive = !crashes.length ? 'active' : '';

    return <div className={'mgbottom20'}>
        <ul className={'unstyled'}>
            {crashes.length
                ? <li>Имеются активные аварии, влияющие на работу интернета.</li>
                : null
            }
            {crashesHistory.length
                ? <li>Недавно были аварии, влиявшие на работу интернта.</li>
                : null
            }
        </ul>
            <ul className="nav nav-tabs" style={{margin: '0px', border: 'none'}}>
                {crashes.length
                    ? <li className="active"><a href="#actual" data-toggle="tab" style={{border: 'none'}}>Актуальные</a></li>
                    : null
                }
                {crashesHistory.length
                    ? <li className={isActive}><a href="#history" data-toggle="tab" style={{border: 'none'}}>История</a></li>
                    : null
                }
            </ul>
            <div className="tab-content" style={{backgroundColor: '#fff'}}>
                {crashes.length
                    ? <div className="tab-pane active" id="actual">
                        <table className={'table'}>
                            <TableHeader isActual={true}/>
                            <tbody>{crashTable(crashes, true)}</tbody>
                        </table>
                    </div>
                    : null
                }
                {crashesHistory.length
                    ? <div className={`tab-pane ${isActive}`} id="history">
                        <table className={'table'}>
                            <TableHeader isActual={false}/>
                            <tbody>{crashTable(crashesHistory, false)}</tbody>
                        </table>
                    </div>
                    : null
                }
            </div>
    </div>
};

/**
 * Заголовок таблицы
 * @param isActual
 * @returns {*}
 * @constructor
 */
const TableHeader = ({isActual}) => <thead>
<tr>
    <th style={{textAlign: 'center'}}>Тикет</th>
    <th style={{width: '90px'}}>Обнаружено</th>
    {isActual
        ? <th style={{width: '75px'}}></th>
        : <th style={{width: '90px'}}>Решено</th>
    }
</tr>
</thead>;

/**
 * Наполнение таблицы. строки с савариями
 * @param props
 * @returns {*}
 * @constructor
 */
const TableRow = (props) => <tr key={props.crash.externalId}>
    <td><a href={`${CrmConfig.urls.jira}browse/${props.crash.externalId}`}
           target={'_blank'}>{props.crash.description}</a></td>
    <td>{moment(props.crash.started).format('DD.MM.YY HH:mm')}</td>
    {props.isActual
        ? <td><CreateTicketLink {...props}/>
        </td>
        : <td>{moment(props.crash.finished).format('DD.MM.YY HH:mm')}</td>
    }
</tr>;

/**
 * Компонент - ссылка на создание тикета в Jira
 * @param props
 * @returns {*}
 * @constructor
 */
const CreateTicketLink = (props) => props.crash.issueTypeId === SWITCH_CRASH_TYPE
    ? <a target={'_blank'} className={'btn btn-mini btn-primary'}
         href={`${TECHSUP_URL(props.contractNumber)}/269/${props.subscriptionId}?preset=no_ping_crash_found&pkey=${props.crash.externalId}&autoTherapy=1&description=${SUBTASK_DESCRIPTION}`}>Создать
        подзадачу</a>
    : <a target={'_blank'} className={'btn btn-mini btn-primary'}
         href={`${TECHSUP_URL(props.contractNumber)}/39/${props.subscriptionId}?preset=mass_failure_in_element&failure_pkey=${props.crash.externalId}`}>Создать
        тикет</a>;

