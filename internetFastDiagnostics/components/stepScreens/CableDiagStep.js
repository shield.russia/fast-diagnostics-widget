import React, {Fragment, useEffect, useState} from 'react';
import {DiagnosticResult} from "../cableDiagnostic/DiagnosticResult";
import {Resolution} from "../Resolution";
import {diagVlApi} from "../../../request/DiagVlApi";
import Spinner from "../../../../Spinner";
import {RESOURCES} from "../../helpers";
import {TECHSUP_URL, NO_INTERNET_TICKET_ID} from "../../../../helpers/JiraLink";
import AutoResurrection from "../cableDiagnostic/AutoResurrection";
import RouterControl from "../cableDiagnostic/RouterControl";
import {crmRPC} from "../../../request/CrmRPC";

/**
 * Шаг быстрой диагностики
 * Опрос fast-port-diag api
 * вывод результата
 * вывод кнопки "Автолечения" либо кнопки перезагрузки роутера
 * @param props
 * @returns {null}
 * @constructor
 */
export const CableDiagStep = (props) => {

    let timer;

    const {
        fastDiag: {
            result,
            isLoading,
            isError,
            setResult,
            setLoading,
            setError,
            hasRouter,
            routerState,
            checkRouter,
            setCache
        },
        crashesProps: {crashes},
        startDiag,
        subscriptionId,
        contractNumber,
        ipAddress
    } = props;

    const [errorsRefresh, setErrorsRefresh] = useState(false);
    const [linkRefresh, setLinkRefresh] = useState(false);

    useEffect(() => {
        if (!result && !isLoading){
            diagVlApi.runFastDiag(subscriptionId, RESOURCES);
        }
    }, [subscriptionId]);

    useEffect(() => {
        if (!result || !result.theEnd){
            timer = setInterval(() => loadDiagnosticResult(), 2000);
            loadDiagnosticResult();
        }
    }, [isLoading]);

    function onRefresh(){
        setLoading(true);
        setResult(null);
        startDiag();
        checkRouter();
    }

    function loadDiagnosticResult() {
        diagVlApi
            .getFastDiag(subscriptionId, RESOURCES.join(','))
            .then(handleDiagnosticResult, handleDiagnosticFail);
    }

    const handleDiagnosticResult = (data) => {
        if (data) {
            const {theEnd} = data;
            if (theEnd === true) {
                clearInterval(timer);
                setLoading(false);
            }
            setResult(data);
        }
    };

    const handleDiagnosticFail = (error) => {
        console.warn('Fast diag error:', error);
        setError(error);
        setLoading(false);
    };

    const spinner = <Spinner radius={10}/>;

    /**
     * Возращает список результатов тестов для коммутатора
     * @return {Array}
     */
    function getCommutatorTests() {

        const {diagnostics: {a4}, theEnd} = result;
        const testResult = [];

        if (typeof a4 != 'undefined') {
            testResult.push(<Resolution
                type={a4.isOk ? 'success' : 'important'}>{a4.isOk ? 'Коммутатор: OK' : 'Коммутатор не доступен'}</Resolution>);
        }

        if (!theEnd && result.length !== 1) {
            testResult.push(spinner);
        }

        return testResult;
    }

    /**
     * Возращает список результатов тестов для порта
     * @return {Array}
     */

    function getPortTests() {
        const {diagnostics: {port, vlan, mac, verification}, theEnd} = result;
        const testResult = [];

        if (typeof verification != 'undefined') {
            testResult.push(<Resolution
                type={verification.isOk ? 'success' : 'important'}>Выверен: {verification.isOk ? 'да' : 'нет'}</Resolution>);
        }

        if (typeof port != 'undefined') {
            testResult.push(<Resolution
                type={port.isOk ? 'success' : 'important'}>Порт: {port.isOk ? 'открыт' : 'закрыт'}</Resolution>);
        }

        if (typeof vlan != 'undefined') {
            testResult.push(<Resolution
                type={vlan.isOk ? 'success' : 'important'}>{vlan.isOk ? 'Vlan: OK' : 'Не корректно настроен vlan'}</Resolution>);
        }

        if (typeof mac != 'undefined') {
            testResult.push(<Resolution
                type={mac.isOk ? 'success' : 'important'}>{mac.isOk ? 'MAC: OK' : 'MAC: Ошибка'}</Resolution>);
        }

        if (!theEnd && testResult.length !== 4) {
            testResult.push(spinner);
        }

        return testResult;

    }

    const linkResult = () => {
        const {diagnostics: {link}} = result;
        return linkRefresh
            ? <Resolution>{spinner}</Resolution>
            : <Resolution onClick={() => refreshLink()}
                          style={{cursor: 'pointer'}}
                          type={link.isOk ? 'success' : 'important'}>
                Линк: {link.isOk ? 'UP' : 'DOWN'}&nbsp;<i className="icon-repeat icon-white"/>
            </Resolution>
    };

    function refreshLink() {
        setLinkRefresh(true);
        diagVlApi.runFastDiag(subscriptionId, ['link']).then(handleLinkStatus);
    }

    function handleLinkStatus(response) {
        const {diagnostics: {link}} = response;
        setLinkRefresh(false);
        setResult({...result, diagnostics: {...result.diagnostics, link}})
    }

    function refreshErrors() {
        setErrorsRefresh(true);
        diagVlApi.runFastDiag(subscriptionId, ['errors']).then(handlePortErrors);
    }

    function handlePortErrors(response) {
        const {diagnostics: {errors}} = response;
        setErrorsRefresh(false);
        setResult({...result, diagnostics: {...result.diagnostics, errors}})
    }

    const renderErrors = () => {
        const {diagnostics: {errors}} = result;
        return errorsRefresh
            ? <Resolution>{spinner}</Resolution>
            : <Resolution
                onClick={() => refreshErrors()}
                style={{cursor: 'pointer'}}
                type={errors.isOk ? 'success' : 'important'}>
                {errors.isOk ? 'Ошибок нет' : `Ошибки: ${errors.result}`}
                &nbsp;
                <i className="icon-repeat icon-white"/>
            </Resolution>
    };

    /**
     * Возращает список результатов тестов для кабеля
     * @return {Array}
     */
    function getCableTests() {
        const {diagnostics: {duplex, errors, negotiation, link}, theEnd} = result;
        const testResult = [];

        if (typeof link != 'undefined') {
            testResult.push(linkResult());
        }

        if (typeof negotiation != 'undefined') {
            testResult.push(<Resolution
                type={negotiation.isOk ? 'success' : 'important'}>Согласование: {negotiation.result}</Resolution>);
        }

        if (typeof duplex != 'undefined') {
            testResult.push(<Resolution
                type={duplex.isOk ? 'success' : 'important'}>Duplex: {duplex.result}</Resolution>);
        }

        if (typeof errors != 'undefined') {
            testResult.push(renderErrors());
        }

        if (!theEnd && testResult.length !== 4) {
            testResult.push(spinner);
        }

        return testResult;
    }

    /**
     * Возвращает список предупреждений
     * @return {Array}
     */
    function getAlerts() {
        if (result === null) {
            return [];
        }

        const {diagnostics, exception} = result;

        let alerts = [];

        Object.entries(diagnostics).forEach(([key, value]) => {
            if (value.isOk !== true) {
                alerts.push(prepareDescription(value, key))
            }
        });

        if (hasRouter && routerState){
           if (alerts.length){
               crmRPC.setCacheData('*Результат быстрой диагностики:* \\\\ ' + alerts.join(' \\\\ '), `${contractNumber}_${subscriptionId}-${ipAddress}--${NO_INTERNET_TICKET_ID}`, 1800),
               setCache(true);
           }
           alerts = [];
           alerts.push(prepareDescription({description: 'Роутер доступен, если абонент не подтверждает наличие связи - перезагрузите роутер'}, 'router'));
        }

        if (exception !== null) {
            alerts.push(`ОШИБКА ДИАГНОСТИКИ: ${exception}`);
        }

        return alerts;
    }

    function prepareDescription(item, itemName) {
        const {exception} = result;
        const {description} = item;

        if (itemName === 'a4') {
            return !exception
                ? <Fragment>{description} <br/>{renderCrashTicketLink()}</Fragment>
                : exception
        }

        return description
    }

    function renderCrashTicketLink() {
        const {contractNumber, subscriptionId} = props;

        return getCrashTicket()
            ?
            <a href={`${TECHSUP_URL(contractNumber)}/269/${subscriptionId}?preset=no_ping_crash_found&pkey=${getCrashTicket()}`}>создать
                подзадачу</a>
            : <a href={`${TECHSUP_URL(contractNumber)}/39/${subscriptionId}?preset=no_ping`}>создать тикет</a>;
    }

    function getCrashTicket() {
        const switchCrash = crashes.filter(crash => crash.issueTypeId === 266);

        return crashes && switchCrash.length > 0 ? switchCrash[0].issueKey : false;
    }

    function renderAlerts(alerts) {
        return alerts.map(alert => <div className="alert alert-block mgtop5" key={alert}>{alert}</div>);
    }


    function getTestResult() {

        return [
            {
                title: 'Коммутатор:',
                tests: getCommutatorTests()
            },
            {
                title: 'Порт:',
                tests: getPortTests()
            },
            {
                title: 'Кабель:',
                tests: getCableTests()
            }
        ];
    }

    const alerts = getAlerts();

    return result !== null
        ? <div className="mgtop10">
            <RefreshButton isVisible={!isLoading} handleClick={() => onRefresh()}/>
            <DiagnosticResult testResult={getTestResult()}/>
            {alerts && renderAlerts(alerts)}
            {!hasRouter || (hasRouter && !routerState)
                ? <AutoResurrection {...props}/>
                : <RouterControl {...props}/>
            }

        </div>
        : null

};

const RefreshButton = (props) => props.isVisible ? <button className={'btn btn-mini mgbottom10'} onClick={() => props.handleClick()}><i className="icon-refresh"/>&nbsp;Запустить повторно</button> : null;


