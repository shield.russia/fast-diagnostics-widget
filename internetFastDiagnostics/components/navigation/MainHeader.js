import React from 'react';

/**
 * Кнопка свернуть / развернуть блок диагностики
 * @param props
 * @returns {*}
 * @constructor
 */
export default function MainHeader(props) {
    const {isOpen, handleClick} = props;

    return <div className={'mgbottom10'}>
        <b>Состояние соединения</b>
        <button type="button" className={`btn pull-right diag-button`}
                onClick={() => handleClick(!isOpen)}>{isOpen ? 'Свернуть' : 'Проверить'}</button>
    </div>

}