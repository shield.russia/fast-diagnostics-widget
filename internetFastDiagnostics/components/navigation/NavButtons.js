import React from 'react';

/**
 * Кнопки навигации в нижней части компонента
 * @param props
 * @returns {*}
 * @constructor
 */
export const NavButtons = (props) => <div>
    <button className={'btn diag-button'} onClick={() => props.previous()} disabled={props.isFirst}>Назад</button>
    <button className={'btn diag-button pull-right'} onClick={() => props.next()} disabled={props.isLast}>Далее</button>
</div>;