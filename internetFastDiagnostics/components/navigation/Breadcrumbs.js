import React from 'react';
import {getScreenById} from "../../constants/ScreensMap";

/**
 * Навигация между шагами "Хлебные крошки"
 * @param props
 * @returns {*}
 * @constructor
 */
export const Breadcrumbs = (props) => {
    const navLine = props.open.map(id => {
        const screen = getScreenById(id);
        const delimiter = props.open.includes(id + 1) ? ' >' : null;
        return <li key={id} style={{paddingLeft: '0px'}}>{
            id === props.selected
                ? <span>{screen.title} {delimiter} </span>
                : <span><a onClick={() => props.set(id)}>{screen.title}</a> {delimiter} </span>
        }</li>

    });

    return <ul className={'inline unstyled'} style={{color: '#757575', fontSize: '12px'}}>{navLine}</ul>
};



