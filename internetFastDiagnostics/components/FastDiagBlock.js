import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import MainHeader from "./navigation/MainHeader";
import {Breadcrumbs} from "./navigation/Breadcrumbs";
import StepScreen from "./StepScreen";
import {NavButtons} from "./navigation/NavButtons";
import {RESOURCES} from "../helpers";
import {diagVlApi} from "../../request/DiagVlApi";

/**
 * Основной компонент. используется в карточке абонента
 * Вызывается из контейнера
 * В пропсы передаются данные из стора карточки
 * @param props
 * @returns {*}
 * @constructor
 */

export default function FastDiagBlock(props) {

    const {subscriptionId} = props;

    const [stepsDisplay, showSteps] = useState(false);
    const [selectedStep, setStep] = useState(0);
    const [openSteps, addStep] = useState([]);
    const [lastStep, setLast] = useState(0);

    useEffect(() => addBreadcump(selectedStep), [selectedStep]);

    function openBlock(isOpen) {
        showSteps(isOpen);
        isOpen && startDiag()
    }

    function startDiag(){
        diagVlApi.runFastDiag(subscriptionId, RESOURCES);
    }

    function addBreadcump(step) {
        if (!openSteps.includes(step)){
            addStep([...openSteps, step])
        }
    }

    function removeBreadcump(step) {
        addStep(openSteps.filter(item => item !== step))
    }

    function nextStep() {
        setStep(selectedStep + 1);
    }

    function previousStep() {
        setStep(selectedStep - 1);
    }

    const isFirst = !openSteps.includes(selectedStep - 1);

    const isLast = selectedStep === lastStep;

    return <div className={`well`}>
        <MainHeader handleClick={(isOpen) => openBlock(isOpen)} isOpen={stepsDisplay}/>
        { stepsDisplay && <Breadcrumbs
            selected={selectedStep}
            open={openSteps}
            set={(stepCount) => setStep(stepCount)}
        />
        }
        { stepsDisplay && <StepScreen
            {...props}
            selected={selectedStep}
            startDiag={() => startDiag()}
            set={(stepCount) => setStep(stepCount)}
            setLast={(step) => setLast(step)}
            remove={(stepNumber) => removeBreadcump(stepNumber)}
        />
        }
       { stepsDisplay && <NavButtons
           {...props}
           isFirst = {isFirst}
           isLast = {isLast}
           selected={selectedStep}
           next={() => nextStep()}
           previous={() => previousStep()}
       />
       }
    </div>

}

FastDiagBlock.propTypes = {
    switchName: PropTypes.string.isRequired,
    port: PropTypes.string.isRequired,
    vlan: PropTypes.string.isRequired,
    contractNumber: PropTypes.string.isRequired,
    ipAddress: PropTypes.string.isRequired,
    subscriptionId: PropTypes.number.isRequired,
    buttonClasses: PropTypes.string
};

